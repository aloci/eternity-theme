### Usage of Config.yaml

Example config is here: [**config.yaml**](/config.example.yaml)

You can use `title`, `subtitle` and `image` variables for your website. Same variables are available for post pages.
```yaml
title: Eternity
params:
  subtitle: IN ICTU OCULI photography
  image: '/images/about.png`
```

You can use `googleAnalytics` variable to set Analytics.
```yaml
googleAnalytics: ''
```

You can use `plausible` variable to set Plausible.
```yaml
params:
  plausible: ''
```

You can use `copyright` and `author` variables to set Copyright notice.
```yaml
params:
  copyright: ''
  author: ''
```

You can set hidden for Source notice. But, please do not do this.
```yaml
params:
  dontShowSource: false
```

You can change columns count for desktop and mobile separately. Default value will be used if value is not defined for page type.
```yaml
params:
  portfolio:
    columns:
      desktop:
        limits: 4
        archive: 6
        people: 2
        default: 3
      mobile:
        default: 2
        archive: 3
        people: 1
```

You can use `socials` array to set your social accounts. `icon` is font-awesome icon code. You can use `landing` variable to make icon invisible only for landing page but visible inside website.
```yaml
params:
  socials:
    - icon: 'far fa-envelope fa-lg'
      url: 'mailto:eternity@iio'
      landing: true
    - icon: 'fab fa-instagram fa-lg'
      url: 'https://instagram.iio/eternity'
    - icon: 'fab fa-linkedin-in fa-lg'
      url: 'https://linkedin.iio/in/eternity'
```

You can change `homepage` link.
```yaml
params:
  homepage: "/intro"
```

You can change special page names.
```yaml
params:
  specialPages:
    - intro
    
```

You can bypass welcome page if you want. It redirects "/" to "homepage".
```yaml
params:
  bypassWelcomePage: true
```

You can disable logo's radius with `disableRadius` variable.
```yaml
params:
  disableRadius: true
```

You can use `menu.main` array to set your navbar links.
```yaml
menu:
  main:
    - name: intro
      url: /intro/
      weight: 1
    - name: people
      url: /tags/people/
      weight: 2
    - name: limits
      url: /tags/limits/
      weight: 3
    - name: archive
      url: /tags/archive/
      weight: 4
    - name: about
      url: /about/
      weight: 5
```
